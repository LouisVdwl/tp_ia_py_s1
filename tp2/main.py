import math

import graphe as g


def dijkstra(graphe, depart, arrivee):
    # Recuperer les arrêtes du graphes
    edges = graphe.edges
    e = {}
    d = {}
    prec = {}
    # Ajouter tous les edges dans le dico
    for i in range(1, len(edges)):
        e[i] = edges[i]
    # parcourir tous les edges dans e
    for a in e:
        d[a] = math.inf  # definir la distance avec le départ
        prec[a] = None
    d[depart] = 0
    while len(e) != 0:
        u = min(e, d)
        if u == arrivee:
            return d, prec
        temp = e[u]
        del e[u]
        for vd in temp:
            v = vd[0]
            alt = d[u] + vd[1]
            if alt < d[v]:
                d[v] = alt
                prec[v] = u


def min(e, d):
    min = math.inf
    for elt in e:
        if d[elt] < min:
            min = d[elt]
            ret = elt
    return ret


def getRoute(dico, depart, arrivee):
    dptCourant = arrivee
    ret = []
    while dptCourant != depart:
        temp = dptCourant
        dptCourant = dico[dptCourant]
        ret.append([dptCourant, temp])
    return ret


if __name__ == '__main__':
    depart = 59
    arrivee = 13
    graphe = g.lire_france()
    c = (getRoute(dijkstra(graphe, depart, arrivee)[1], depart, arrivee))
    print(c)
    g.voir_chemins(graphe, c)