import data_arbres as d
import math


def proba_empirique(d):
    e = {}
    somme = 0
    for elt in d:
        if elt.y in e:
            e[elt.y] += 1
        else:
            e[elt.y] = 0
    for elt in e:
        somme += e[elt]
    for elt in e:
        e[elt] = e[elt] / somme
    return e


def question_inf(x, a, s):
    return x.x[a] < s


def split_data(d, a, s):
    d1, \
    d2 = []
    for elt in d:
        d1.append(elt) if question_inf(elt, a, s) else d1.append(elt)
    return d1, d2


def list_separ_attributs(d, a):
    ret = []
    listea = list(set([elt.x[a] for elt in d]))
    listea.sort()
    for i in range(len(listea) - 1):
        ret.append(('Age', (listea[i] + listea[i + 1]) / 2))
    return ret


def liste_question(d):
    result = []
    for elt in d[0].x:
        result.append(list_separ_attributs(d, elt))
    return result


def proba_empirique(datas):
    results = {}
    total = 0
    for data in datas:
        try:
            results[data.y] += 1
        except:
            results[data.y] = 1
        total += 1
    return {result: results[result] / total for result in results}


def question_inf(x, a, s):
    try:
        return x.x[a] < s
    except:
        return x[a] < s


def split(datas, a, s):
    d1 = []
    d2 = []
    for data in datas:
        if question_inf(data, a, s):
            d1.append(data)
        else:
            d2.append(data)

    return (d1, d2)


def list_separ_attributs(datas, a):
    tab = []
    for data in datas:
        tab.append(data.x[a])
    tab = list(set(tab))
    tab.sort()
    result = []
    for i in range(1, len(tab)):
        result.append((a, ((tab[i] + tab[i - 1]) / 2)))

    return result


def liste_questions(datas):
    result = []
    for att in datas[0].x:
        result.append(list_separ_attributs(datas, att))
    return result


def entropie(datas):
    Hd = None
    probas = proba_empirique(datas)
    for i in probas:
        pi = probas[i]
        if (pi == 0):
            Hd = Hd - 0
            break

        if (Hd == None):
            Hd = - pi * math.log(pi)
        else:
            Hd = Hd - pi * math.log(pi)
    return Hd


def gain_entropie(datas, question):
    (d1, d2) = split(datas, question[0], question[1])
    toto = len(d1) + len(d2)
    return entropie(datas) - (len(d1) / toto) * entropie(d1) - (len(d2) / toto) * entropie(d2)


def best_split(datas):
    max = -math.inf
    bestQuestion = None
    for questions in liste_questions(datas):
        for question in questions:
            gain = gain_entropie(datas, question)
            if max < gain:
                max = gain
                bestQuestion = question
    return bestQuestion


def precision(noeud, datas):
    i = 0
    good_prediction = 0
    for data in datas:
        i += 1
        prediction = noeud.prediction(data)
        if 0 in prediction.keys():
            prediction_number = prediction[0]
        else:
            prediction_number = prediction[1]
        if prediction_number == data.y:
            good_prediction += 1
    return good_prediction / i

def comparatif(noeud20, datas20, noeud80, datas80):
    precision20 = precision(noeud20, datas20)
    precision80 = precision(noeud80, datas80)
    return 0

def precision_profondeur(profondeur_max):
    Node = d.Noeud(0,profondeur_max)
    Node.grow(datas80)
    return precision(Node,datas20),precision(Node,datas80)

def get_results():
    result_test = []
    result_app = []
    for x in range(1, 20):
        val = precision_profondeur(x)
        result_test.append(val[0])
        result_app.append(val[1])
    return result_test, result_app



if __name__ == '__main__':
    datas = d.load_data('tp_donnees.csv')
    noeud = d.Noeud(proba_empirique(datas))
    noeud.grow(datas)
    # print(precision(noeud, datas))

    count20 = round(0.2*len(datas))
    count80 = len(datas) - count20
    datas20 = datas[:count20]
    datas80 = datas[-count80:]

    print(get_results())

