import numpy as np
import main as function

class DataPoint:
    def __init__(self, x, y, cles):
        self.x = {}
        for i in range(len(cles)):
            self.x[cles[i]] = float(x[i])
        self.y = int(y)
        self.dim = len(self.x)
        
    def __repr__(self):
        return 'x: '+str(self.x)+', y: '+str(self.y)


def load_data(filelocation):
    with open(filelocation,'r') as f:
        data = []
        attributs = f.readline()[:-1].split(',')[:-1]
        for line in f:
            z = line.split(',')
            if z[-1] == '\n':
                z = z[:-1]
            x = z[:-1]
            y = int(z[-1])
            data.append(DataPoint(x,y,attributs))
    return data


class Noeud:
    def __init__(self,proba, profondeur_max=np.infty,hauteur=0):
        self.question = None
        self.enfants = {}
        self.profondeur_max = profondeur_max
        self.proba = proba
        self.hauteur = hauteur

    def prediction(self, x):
        if self.question is None:
            return self.proba
        else:
            enfant = self.enfants[function.question_inf(x, self.question[0], self.question[1])]
            return enfant.prediction(x)

    def grow(self, datas):
        result = function.best_split(datas)
        if result is not None:
            split = function.split(datas,result[0],result[1])
            if len(split[0]) !=0 and len(split[1]) !=0 and function.gain_entropie(datas,result):
                self.question = result
                noeud1 = Noeud(proba=function.proba_empirique(split[0]),hauteur=self.hauteur+1)
                noeud2 = Noeud(proba=function.proba_empirique(split[1]),hauteur=self.hauteur+1)
                noeud1.grow(split[0])
                noeud2.grow(split[1])
                self.enfants[True] = noeud1
                self.enfants[False] = noeud2