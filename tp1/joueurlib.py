import math

import numpy.random

import interfacelib
import numpy as np


class Joueur:
    def __init__(self, partie, couleur, opts={}):
        self.couleur = couleur
        self.couleurval = interfacelib.couleur_to_couleurval(couleur)
        self.jeu = partie
        self.opts = opts

    def demande_coup(self):
        pass


class Humain(Joueur):

    def demande_coup(self):
        pass


class IA(Joueur):

    def __init__(self, partie, couleur, opts={}):
        super().__init__(partie, couleur, opts)
        self.temps_exe = 0
        self.nb_appels_jouer = 0


class Random(IA):
    def demande_coup(self):
        jeu = self.jeu
        plateau = jeu.plateau
        liste_coups_valides = plateau.liste_coups_valides(self.couleurval)
        coup = liste_coups_valides[numpy.random.randint(0, len(liste_coups_valides))]
        return coup


class Minmax(IA):

    def demande_coup(self):
        # Recuperation du plateau et des cases
        jeu = self.jeu

        # Copier le plateau pour simulation
        plateau_copie = self.jeu.plateau.copie()

        return self.minMax(plateau_copie, self.couleurval, 0)[1]

    def minMax(self, plateau, joueur, i):
        adversaire = - joueur
        if not plateau.existe_coup_valide(joueur) or i >= 2:
            return self.fonction_evaluation(self.couleurval, plateau), []

        if joueur == self.couleurval:
            m = - math.inf
            for coupCourant in plateau.liste_coups_valides(joueur):
                copie_plateau = plateau.copie()
                copie_plateau.jouer(coupCourant, joueur)
                val = self.minMax(copie_plateau, adversaire, i + 1)[0]
                if m < val:
                    m = val
                    return_coup = coupCourant
            return val, return_coup
        else:
            m = math.inf
            for coupCourant in plateau.liste_coups_valides(adversaire):
                copie_plateau = plateau.copie()
                copie_plateau.jouer(coupCourant, adversaire)
                val = self.minMax(copie_plateau, self.couleurval, i + 1)[0]
                if m > val:
                    m = val
                    return_coup = coupCourant
            return val, return_coup

    def fonction_evaluation(self, couleur, plateau):
        # Recuperation du plateau et des cases
        cases = plateau.tableau_cases

        # Initialisation du nombre de pions noirs / blancs
        noir = 0
        blanc = 0

        # Calcul du nombre de cases noirs / blancs
        for elt in cases:
            for couleur in elt:
                if couleur != 0:
                    if couleur == 1:
                        noir += 1
                    else:
                        blanc += 1
        # Retourner la différence entre les deux couleurs selon la couleur du joueur courant
        return noir - blanc if couleur == 1 else blanc - noir


class AlphaBeta(IA):

    def demande_coup(self, profondeur):
        return self.alpha_beta(self.jeu.plateau, profondeur, self.couleurval, -np.infty, np.infty)[1]

    def alpha_beta(self, plateau, profondeur, couleur, alpha, beta):
        if profondeur == 0:
            return self.position(plateau, couleur), []
        if couleur == self.couleurval:
            score_normal = -np.infty
            liste = self.jeu.plateau.liste_coups_valides(couleur)
            for coup_valide in liste:
                copie_plateau = plateau.copie()
                copie_plateau.jouer(coup_valide, couleur)
                res = self.alpha_beta(copie_plateau, profondeur - 1, -couleur, alpha, beta)[0]
                score_copie = res
                if score_normal < score_copie:
                    score_normal = score_copie
                    meilleur_coup = coup_valide
                    alpha = max(alpha, score_normal)
                if alpha >= beta:
                    break
            return (score_normal, meilleur_coup)
        else:
            score_normal = np.infty
            liste = self.jeu.plateau.liste_coups_valides(couleur)
            for coup_valide in liste:
                copie_plateau = plateau.copie()
                copie_plateau.jouer(coup_valide, couleur)
                res = self.alpha_beta(copie_plateau, profondeur - 1, -couleur, alpha, beta)[0]
                score_copie = res
                if score_normal > score_copie:
                    score_normal = score_copie
                    meilleur_coup = coup_valide
                    beta = max(beta, score_normal)
                if alpha >= beta:
                    break
            return (score_normal, meilleur_coup)

    def position(self, plateau, joueur):
        noir = 0
        blanc = 0
        for ligne in plateau.tableau_cases:
            for case in ligne:
                if case == -1:
                    blanc += 1
                elif case == 1:
                    noir += 1
        return noir - blanc if joueur == 1 else blanc-noir
