import matplotlib.pyplot as plt
import jeulib

def printMinMaxGraph(profondeurs):
    compteurs = []
    times = []
    for profondeur in profondeurs:
        jeu = jeulib.Jeu()
        jeu.demarrer(profondeur)
        compteurs.append(jeu.compteur[1])
        times.append(jeu.timeEnd - jeu.timeStart)
    plt.xlabel("Profondeurs")
    plt.plot(profondeurs,compteurs, label="Compteurs Jouer")
    plt.plot(profondeurs,times, label="Temps")
    plt.legend()

    plt.show()
