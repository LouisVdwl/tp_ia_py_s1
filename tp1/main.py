import grapheMinMax
import jeulib
import joueurlib

if __name__ == "__main__":
    jeu = jeulib.Jeu()
    jeu.blanc = joueurlib.Minmax
    jeu.noir = joueurlib.Minmax
    jeu.demarrer(2)
    grapheMinMax.printMinMaxGraph([1, 2, 3, 4])
